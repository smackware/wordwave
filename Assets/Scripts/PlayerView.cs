﻿using UnityEngine;
using System.Collections;

public class PlayerView : MonoBehaviour {


	[SerializeField]
	private Sprite[] _displaySpriteIdle = null;

	[SerializeField]
	private Sprite[] _displaySpriteAttack = null;


	[SerializeField]
	private Sprite[] _displaySpriteRangedAttack = null;

	[SerializeField]
	private Transform _projectileEmissionPivot = null;

	[SerializeField]
	private SpriteRenderer _spriteRenderer = null;

	[SerializeField]
	private GameObject _spritesPivot = null;

	[SerializeField]
	private Transform _flipPivot = null;

	[SerializeField]
	private EnemyGibView _gibPrefab = null;

	private bool _playWalkAnim = true;

	public void EmitGib(string text)
	{
		var gibView = Instantiate (_gibPrefab);
		gibView.SetText (text);
		gibView.Shoot (transform.position);
	}

	public bool FaceRight
	{
		get {
			return _flipPivot.localScale.x > 0;
		}
		set {
			_flipPivot.localScale = value ? Vector3.one : new Vector3 (-1, 1, 1);
		}
	}

	public Vector3 Position
	{
		get {
			return transform.position;
		}
		set
		{
			transform.position = value;
		}
	}

	public Vector3 ProjectileEmissionPosition
	{
		get
		{
			return _projectileEmissionPivot.position;
		}
	}

	public void PlayIdle()
	{
		_playWalkAnim = true;
	}

	public void PlayMeleeAttack()
	{
		_playWalkAnim = false;
		SetRandomSprite (_displaySpriteAttack);
	}


	public void PlayRangedAttack()
	{
		_playWalkAnim = false;
		StartCoroutine (PlayRangedAttackCoroutine ());
	}

	private IEnumerator PlayRangedAttackCoroutine()
	{
		SetRandomSprite (_displaySpriteRangedAttack);
		yield return new WaitForSeconds(0.3f);
		PlayIdle ();
	}

	private void SetRandomSprite(Sprite[] sprites)
	{
		_spriteRenderer.sprite = sprites [Random.Range (0, sprites.Length)];
	}


	public void Blink()
	{
		StartCoroutine (BlinkCoroutine ());
	}

	private IEnumerator BlinkCoroutine()
	{
		for (int i = 0; i < 3; i++) {
			yield return new WaitForSeconds (0.1f);
			_spritesPivot.SetActive (false);
			yield return new WaitForSeconds (0.1f);
			_spritesPivot.SetActive (true);
		}
	}

	private void Update()
	{
		if (!_playWalkAnim) {
			return;
		}
		var currentWalkFrame = (int)((Time.time * 7)) % _displaySpriteIdle.Length;
		_spriteRenderer.sprite = _displaySpriteIdle [currentWalkFrame];
	}
}
