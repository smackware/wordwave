﻿using UnityEngine;
using System.Collections;

public class HpBar : MonoBehaviour 
{
	[SerializeField]
	private GameObject[] _barItems = null;

	public void SetValue(int amount)
	{
		for (int i = 0; i < _barItems.Length; i++) {
			_barItems [i].SetActive (i < amount);
		}
	}
}
