﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EnemyView : MonoBehaviour 
{
	[SerializeField]
	private float _wobbleDuration = 1f;

	[SerializeField]
	private float _wobbleMagnitude = 1f;

	[SerializeField]
	private AnimationCurve _wobbleCurve = AnimationCurve.Linear(0,1,1,1);

	[SerializeField]
	private Transform _wobblePivot = null;

	[SerializeField]
	private Text _displayText = null;

	[SerializeField]
	private EnemyGibView _gibPrefab = null;

	[SerializeField]
	private Transform _displayCanvase = null;

	[SerializeField]
	private RectTransform _displayRectTransform = null;

	[SerializeField]
	private SpriteRenderer _spriteRenderer = null;


	private IEnumerator Start()
	{
		var startColor = Color.white;
		startColor.a = 0;
		_spriteRenderer.color = startColor;
		while (startColor.a < 1) {
			startColor.a += Time.deltaTime * 3;
			yield return null;
			_spriteRenderer.color = startColor;
		}
	}

	public Sprite Sprite
	{
		get { return _spriteRenderer.sprite; }
	}

	public void SetWobbleDuration(float duration)
	{
		_wobbleDuration = duration;
	}

	public void EmitGib(string text)
	{
		var gibView = Instantiate (_gibPrefab);
		gibView.SetText (text);
		gibView.Shoot (transform.position);
	}
		
	public void SetDisplaySprite(Sprite sprite)
	{
		_spriteRenderer.sprite = sprite;
	}

	public void SetDisplayText(string text)
	{
		_displayText.text = text;
	}

	public void SetTextColor(Color color)
	{
		_displayText.color = color;
	}

	public Rect GetCanvasRect()
	{
		return _displayRectTransform.GetWorldRect (Vector2.one * 0.08f);
	}

	public Vector3 CanvasPosition
	{
		get
		{
			return _displayCanvase.position;
		}
		set {
			_displayCanvase.position = value;
		}
	}

	public Vector3 Position
	{
		get
		{
			return transform.position;
		}	
		set 
		{
			transform.position = value;
		}
	}


	private void Update()
	{
		var currentWobbleProgress = (Time.time % _wobbleDuration) / _wobbleDuration;
		var scale = _wobblePivot.localScale;
		scale.y = _wobbleCurve.Evaluate (currentWobbleProgress) * _wobbleMagnitude;
		_wobblePivot.localScale = scale;
	}
}
