﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OnScreenKeyboard : MonoBehaviour 
{
	[SerializeField]
	private GameObject _errorOverlay = null;

	public event Action<string> InputKeyPressed = delegate {};
	public event Action BackspacePressed = delegate {};

	[SerializeField]
	private Text _displayText = null;

	public bool IsInputEnabled { get; set; }

	public void SetDisplayText(string text)
	{
		_displayText.text = text;
	}

	public void Blink()
	{
		StartCoroutine(BlinkCoroutine());
	}

	private IEnumerator BlinkCoroutine()
	{
		
		for (int i = 0; i < 2; i++) {
			yield return new WaitForSeconds (0.1f);
			_errorOverlay.SetActive (true);
			yield return new WaitForSeconds (0.1f);
			_errorOverlay.SetActive (false);
		}
	}
		
	/// <summary>
	/// Wired to UI buttons
	/// </summary>
	/// <param name="input">Input.</param>
	public void KeyPressedCallback(string input)
	{
		if (!IsInputEnabled) 
		{
			return;
		}
		InputKeyPressed (input);
	}

	/// <summary>
	/// Wired to UI Input
	/// </summary>
	public void BackspacePressedCallback()
	{
		if (!IsInputEnabled) 
		{
			return;
		}
		BackspacePressed ();
	}

	private void Update()
	{
		if (!IsInputEnabled) 
		{
			return;
		}
		foreach (char c in Input.inputString) 
		{
			InputKeyPressed (c.ToString ());
		}
	}
}
