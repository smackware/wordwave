﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EnemyGibView : MonoBehaviour
{
	[SerializeField]
	private Text _gibText = null;

	private float _radialSpeed = 0;

	[SerializeField]
	private float _prefadeDelay = 3;


	[SerializeField]
	private AnimationCurve _fadeOutCurve = AnimationCurve.Linear(0,0,1,1);

	private float _floorTrackingSpeed = 6f;

	private bool _scrollWithFloor;
	// Update is called once per frame
	void Update () 
	{
		if (_scrollWithFloor) {
			transform.position -= Vector3.up * Time.deltaTime * _floorTrackingSpeed;
		}
	}
	public void SetText(string text)
	{
		_gibText.text = text;
	}

	public void Shoot(Vector3 startPosition)
	{
		var landPosition = startPosition + Vector3.right * Random.Range (-20, 20)+ Vector3.up * Random.Range (-1, 1);
		StartCoroutine(ShootCoroutine(startPosition, landPosition, Random.Range(0.2f, 1.2f), Random.Range(2, 6), 2f));
	}

	private IEnumerator ShootCoroutine(Vector3 source, Vector3 target, float duration, float height, float fadeoutDuration)
	{
		var color = _gibText.color;
		var startTime = Time.time;
		float elapsed = 0;
		while (elapsed <= duration) 
		{
			var progress = elapsed / duration;
			var position = Vector3.Lerp (source, target, progress) + Vector3.up * Mathf.Sin (progress * 180 * Mathf.Deg2Rad) * height;
			transform.position = position;
			elapsed += Time.deltaTime;
			transform.Rotate(new Vector3(0, 0, _radialSpeed*Time.deltaTime));
			yield return null;
		}
		_scrollWithFloor = true;
		yield return new WaitForSeconds (_prefadeDelay);
		elapsed = 0;
		startTime = Time.time;
		while (elapsed <= duration) {
			var progress = elapsed / duration;
			color.a = 1 - _fadeOutCurve.Evaluate(progress);
			elapsed += Time.deltaTime;
			_gibText.color = color;
			yield return null;
		}
		Destroy (gameObject);
	}

	// Use this for initialization
	void Start () {
		_radialSpeed = Random.Range (-360, 360);
	}
	

}
