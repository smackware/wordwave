﻿using UnityEngine;
using System.Collections;

public static class HighScoreUtils 
{
	private const string PREFS_KEY = "highscore";

	private static int _lastHighScore = -1;

	public static int LoadHighscore()
	{
		if (_lastHighScore > -1) 
		{
			return _lastHighScore;
		}
		if (!PlayerPrefs.HasKey (PREFS_KEY)) {
			_lastHighScore = 0;
		} else {
			_lastHighScore = PlayerPrefs.GetInt (PREFS_KEY);
		}
		return _lastHighScore;
	}

	public static void WriteIfHighScore(int score)
	{
		var highScore = LoadHighscore ();
		if (score < highScore) 
		{
			return;
		}
		_lastHighScore = score;
		PlayerPrefs.SetInt (PREFS_KEY, score);
		PlayerPrefs.Save ();
	}
}
