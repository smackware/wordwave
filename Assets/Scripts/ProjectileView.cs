﻿using UnityEngine;
using System.Collections;

public class ProjectileView : MonoBehaviour
{
	private const float _shotDuration = 0.3f;

	[SerializeField]
	private AnimationCurve _shotCurve = AnimationCurve.Linear(0,0,1,1);

	[SerializeField]
	private Transform _rotationPivot = null;

	public void Shoot(Vector3 startPosition, Vector3 targetPosition)
	{
		StartCoroutine(ShootCoroutine(startPosition, targetPosition, _shotDuration));
	}

	private IEnumerator ShootCoroutine(Vector3 source, Vector3 target, float duration)
	{
		var startTime = Time.time;
		float elapsed = 0;
		while (elapsed <= duration)
		{
			var progress = elapsed / duration;
			var position = Vector3.Lerp (source, target, _shotCurve.Evaluate(progress));
			transform.position = position;
			elapsed += Time.deltaTime;
			yield return null;
		}
		Destroy (gameObject);
	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (_rotationPivot != null) {
			_rotationPivot.Rotate (Vector3.forward * Time.deltaTime * 90);
		}
	}
}
