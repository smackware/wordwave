﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class MainMenu : MonoBehaviour , IPointerClickHandler
{
	private const string ENABLE_SOUND_TEXT = "SoundOff";
	private const string DISABLE_SOUND_TEXT = "SoundOn";

	[SerializeField]
	private GameObject[] _hideOnPC = null;

	[SerializeField]
	private Color _targetMenuItemColor = Color.red;

	[SerializeField]
	private AudioManager _audioManager = null;

	[SerializeField]
	private GameManager _gameManager = null;

	[SerializeField]
	private OnScreenKeyboard _onScreenKeyboard = null;

	[SerializeField]
	private MainMenuButton[] _menuButtons = null;

	[SerializeField]
	private MainMenuButton _soundButton = null;

	[SerializeField]
	private MainMenuButton _highScoreButton = null;

	[SerializeField]
	private GameObject _tappedPopup = null;

	[SerializeField]
	private GameObject _menuItemsContainer = null;

	private MainMenuButton _currentButton = null;

	private void Start()
	{
		_gameManager.GameEnded += OpenMainMenu;
		OpenMainMenu();
	}

	private void OpenMainMenu()
	{
		#if UNITY_STANDALONE_WIN
		foreach (var go in _hideOnPC)
		{
			go.SetActive(false);
		}
		#endif
		_highScoreButton.OriginalText = _highScoreButton.CurrentText = "HighScore: " + HighScoreUtils.LoadHighscore ();
		HandleSoundButton (false);
		gameObject.SetActive (true);
		_onScreenKeyboard.IsInputEnabled = true;
		_onScreenKeyboard.InputKeyPressed += OnInputKeyPressed;
		ResetMenuButtons ();
		foreach (var menuButton in _menuButtons) {
			menuButton.Tapped += OnItemTapped;
		}
	}

	private void OnItemTapped()
	{
		_tappedPopup.SetActive (true);
		_menuItemsContainer.SetActive (false);
	}

	private void DismissTappedPopup()
	{
		_tappedPopup.SetActive (false);
		_menuItemsContainer.SetActive (true);
	}

	public void OnPointerClick (PointerEventData eventData)
	{
		OnItemTapped ();
	}

	private void CloseMainMenu()
	{
		foreach (var menuButton in _menuButtons) {
			menuButton.Tapped -= OnItemTapped;
		}
		_onScreenKeyboard.IsInputEnabled = false;
		_onScreenKeyboard.InputKeyPressed -= OnInputKeyPressed;
		gameObject.SetActive (false);
	}

	private void OnInputKeyPressed (string input)
	{

		_onScreenKeyboard.SetDisplayText (string.Empty);
		bool didHit = false;
		foreach (var menuButton in _menuButtons) 
		{
			if (!menuButton.gameObject.activeInHierarchy) {
				continue;
			}
			if (menuButton.CurrentText.ToUpper ().StartsWith (input.ToUpper ()) && (_currentButton == menuButton || _currentButton == null)) {
				didHit = true;
				_currentButton = menuButton;
				menuButton.Color = _targetMenuItemColor;
				var gibEmitted = menuButton.CurrentText.Substring (0, input.Length);
				menuButton.CurrentText = menuButton.CurrentText.Substring (input.Length);
				menuButton.EmitGib (gibEmitted);
				_onScreenKeyboard.SetDisplayText (menuButton.CurrentText);
				_audioManager.PlayRangedHit ();
				if (menuButton.CurrentText.Length == 0) 
				{
					ResetMenuButtons ();
					HandleButton (menuButton.ButtonType);
				}
			} 
			else 
			{
				menuButton.Color = Color.white;
				menuButton.CurrentText = menuButton.OriginalText;
			}
		}
		if (!didHit) {
			_currentButton = null;
		}
	}

	private void HandleButton(MainMenuButtonType buttonType)
	{
		switch (buttonType) {
		case MainMenuButtonType.DismissTappedPopup:
			DismissTappedPopup ();
			return;
		case MainMenuButtonType.NewGame:
			CloseMainMenu ();
			_gameManager.StartGame ();
			return;
		case MainMenuButtonType.Sound:
			HandleSoundButton (true);
			return;
		case MainMenuButtonType.Quit:
			Application.Quit ();
			return;
		}
	}

	private void HandleSoundButton(bool toggle)
	{
		var prefsKey = "sound";
		var soundEnabled = false;
		if (!PlayerPrefs.HasKey (prefsKey)) {
			soundEnabled = true;
		} else {
			soundEnabled = PlayerPrefs.GetInt (prefsKey) == 2;
		}
		if (toggle) {
			soundEnabled = !soundEnabled;
		}

		_audioManager.IsEnabled = soundEnabled;
		if (soundEnabled) {
			_soundButton.OriginalText = _soundButton.CurrentText = DISABLE_SOUND_TEXT;
		} else {
			_soundButton.OriginalText = _soundButton.CurrentText = ENABLE_SOUND_TEXT;
		}
		PlayerPrefs.SetInt (prefsKey, soundEnabled ? 2 : 1);
		PlayerPrefs.Save ();
	}

	private void ResetMenuButtons()
	{
		_currentButton = null;
		foreach (var menuButton in _menuButtons) 
		{
			menuButton.Color = Color.white;
			menuButton.CurrentText = menuButton.OriginalText;
			_onScreenKeyboard.SetDisplayText (string.Empty);
		}
	}

}
