﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using UnityEngine;
using UnityEngine.UI;

using Random = UnityEngine.Random;

public class Enemy
{
	public string OriginalWord;
	public string Word;
	public float SpawnTime;
	public float AttackDuration;
	public Vector3 StartPosition;
	public bool IsDead;
	public bool DidHit;
	public EnemyView View;
	public bool IsBoss;
}

public enum EnemyActionType
{
	Move,
	Hit,
	Dying,
	Despawn,
	Frozen,
}

public class TypedText
{
	private List<string> _segments = new List<string>();

	public string Text { get; protected set; }

	public void Add(string segment)
	{
		_segments.Add (segment);
		Text += segment;
	}

	public void DeleteLastSegment()
	{
		if (_segments.Count == 0) 
		{
			return;
		}
		_segments.RemoveAt (_segments.Count - 1);
		Text = string.Join ("", _segments.ToArray());
	}

	public void Clear()
	{
		_segments.Clear ();
		Text = string.Empty;
	}
}

[Serializable]
public class WaveConfig
{
	public float PreDelay = 1f;
	public float MinSpawnDuration = 2f;
	public float MaxSpawnDuration = 3f;
	public int MaxSubWaveScore = 200;
	public float MinMinionDuration = 5;
	public float MaxMinionDuration = 8;
	public int MaxBossScore = 10;
	public Sprite[] EnemySprites;
	public Sprite BossSprite;
	public Sprite BossMinionSpawningSprite;
	public int Subwaves = 3;
}

public class SpawnData
{
	public string Announcement = string.Empty;
	public int MaxScore;
	public float MinMinionDuration;
	public float MaxMinionDuration;
	public Sprite[] Sprites;
	public bool IsBoss;
	public float WaveProgress;
}

[Serializable]
public class WaveManager
{
	public WaveConfig[] WaveConfigs;

	public int CurrentLevel { get; private set; }
	public int CurrentSubWave { get; private set; }
	public float _nextSpawnGameTime = -100;

	public void ResetCounters()
	{
		_nextSpawnGameTime = WaveConfigs[0].PreDelay;
		CurrentLevel = 0;
		CurrentSubWave = 0;
	}

	public SpawnData CalculateCurrentSpawn(float gameTime)
	{
		if (gameTime < _nextSpawnGameTime) 
		{
			return null;
		}

		var currentWaveConfigIndex = Mathf.Min (WaveConfigs.Length - 1, CurrentLevel);
		var currentWaveConfig = WaveConfigs [currentWaveConfigIndex];
		CurrentSubWave = Mathf.Min (currentWaveConfig.Subwaves - 1, CurrentSubWave);
		var announcement = string.Empty;
		if (CurrentSubWave == 0) {
			announcement = "Wave " + (CurrentLevel + 1) + " incoming!";
		}
		var spawnData = new SpawnData () {
			Announcement = announcement,
			MaxScore = currentWaveConfig.MaxSubWaveScore,
			MinMinionDuration = currentWaveConfig.MinMinionDuration,
			MaxMinionDuration = currentWaveConfig.MaxMinionDuration,
			Sprites = currentWaveConfig.EnemySprites,
			WaveProgress = (float)(CurrentSubWave + 1) / currentWaveConfig.Subwaves,
		};
		if (CurrentSubWave >= currentWaveConfig.Subwaves - 1) {
			CurrentSubWave = 0;
			CurrentLevel++;
			var nextWaveConfigIndex = Mathf.Min (WaveConfigs.Length - 1, CurrentLevel);
			var nextWaveConfig = WaveConfigs [nextWaveConfigIndex];
			_nextSpawnGameTime = nextWaveConfig.PreDelay + gameTime;
		}
		else
		{
			CurrentSubWave++;
			_nextSpawnGameTime = Random.Range (currentWaveConfig.MinSpawnDuration, currentWaveConfig.MaxSpawnDuration) + gameTime;
		}
		return spawnData;
	}
}


public class GameManager : MonoBehaviour 
{

	public event Action GameEnded = delegate {};
		
	public readonly static Regex WordRegexp = new Regex("^\\s*[a-zA-Z]+\\s*$");
	private string[] _tempWords = {
		"Hello",
		"World",
		"Kitty",
		"Foo",
	};

	[SerializeField]
	private WaveAnnouncer _waveAnnouncer = null;

	[SerializeField]
	private Color _mainTargetColor = Color.red;

	[SerializeField]
	private Color _regularEnemyColor = Color.white;

	[SerializeField]
	private AudioManager _audioManager = null;

	[SerializeField]
	private TickingCounter _scoreCounter = null;

	[SerializeField]
	private OnScreenKeyboard _onScreenKeyboard = null;

	[SerializeField]
	private Transform[] _enemySpawnPointPivots = null;

	[SerializeField]
	private PlayerView _playerPivot = null;

	[SerializeField]
	private EnemyView _enemyViewPrefab = null;

	[SerializeField]
	private EnemyBodyView _enemyBodyViewPrefab = null;

	[SerializeField]
	private Transform _enemyExitPivot = null;

	[SerializeField]
	private ProjectileView _projectilePrefab = null;

	[SerializeField]
	private bool _samuraiMode = true;

	[SerializeField]
	private CameraShake _cameraXShake = null;

	[SerializeField]
	private CameraShake _cameraYShake = null;

	[SerializeField]
	private WaveManager _waveManager = null;

	[SerializeField]
	private HpBar _hpBar = null;

	[SerializeField]
	private Slider _waveBar = null;

	private Enemy _currentTarget = null;
	private List<Enemy> _currentEnemies = new List<Enemy>();
	private Dictionary<char, List<string>> _wordsByFirstLetter = new Dictionary<char, List<string>>();
	private int _playerHp;
	private int _playerScore = 0;
	private float _enemyExitDuration = 2;
	private float _gameStartTime = -100;
	private float _penaltyEndTime = -100;
	private bool _gameInProgress = false;
	private bool _freezeEnemies;

	private float CurrentGameTime
	{
		get 
		{
			return Mathf.Max (0, Time.time - _gameStartTime);
		}
	}


	protected void Start()
	{
		//StartGame ();
	}


	protected void OnDestroy()
	{
		
	}

	private void IndexWords(IEnumerable<string> words)
	{
		foreach (var word in words) 
		{
			char firstLetter = word.ToUpper () [0];
			if (!_wordsByFirstLetter.ContainsKey (firstLetter)) {
				_wordsByFirstLetter [firstLetter] = new List<string> ();
			}
			_wordsByFirstLetter [firstLetter].Add (word);
		}
	}

	private IEnumerable<string> LoadWords()
	{
		var textAsset = Resources.Load<TextAsset> ("text");
		foreach (var line in textAsset.text.Split ('\n')) 
		{
			foreach (var word in line.Split(',')) {
				if (WordRegexp.IsMatch(word))
				{
					yield return word.Trim ();
				}
			}
		}
	}

	private int PlayerHp
	{
		get{
			return _playerHp;
		}
		set {
			_playerHp = value;
			_hpBar.SetValue (value);
		}
	}

	public void StartGame()
	{
		IndexWords (LoadWords());
		_waveManager.ResetCounters ();
		PlayerHp = 3;
		_gameStartTime = Time.time;
		_gameInProgress = true;
		_penaltyEndTime = -100;
		_playerScore = 0;
		#if UNITY_STANDALONE_WIN
		_waveBar.gameObject.SetActive (false);
		#else
		_waveBar.gameObject.SetActive (true);
		#endif

		_onScreenKeyboard.IsInputEnabled = true;
		_onScreenKeyboard.InputKeyPressed += OnKeyboardInput;
		_onScreenKeyboard.BackspacePressed += OnBackspacePressed;
		_audioManager.PlayMusic (true);
	}

	private void EndGame()
	{
		_waveAnnouncer.gameObject.SetActive (false);
		_waveBar.gameObject.SetActive (false);
		_audioManager.PlayMusic (false);
		_gameInProgress = false;
		_currentTarget = null;
		_onScreenKeyboard.IsInputEnabled = false;
		_onScreenKeyboard.InputKeyPressed -= OnKeyboardInput;
		_onScreenKeyboard.BackspacePressed -= OnBackspacePressed;

		StartCoroutine (EndGameCoroutine());
	}

	private void DespawnAllEnemies()
	{
		while (_currentEnemies.Count  > 0) {
			DestroyEnemy (0);
		}
	}

	private void SpawnNextEnemyOnUpdate()
	{
		var spawnData = _waveManager.CalculateCurrentSpawn (CurrentGameTime);
		if (spawnData == null) {
			return;
		}
		if (spawnData.Announcement != null && spawnData.Announcement != string.Empty) {
			_waveAnnouncer.Show (spawnData.Announcement);
		}
		int totalScore = 0;
		_waveBar.value = spawnData.WaveProgress;
		_enemySpawnPointPivots = _enemySpawnPointPivots.OrderBy (x => Random.Range (0, _enemySpawnPointPivots.Length + 100)).ToArray ();
		int nextTotalScore = 0;
		List<string> words = new List<string> ();
		do {
			totalScore = nextTotalScore;
			var availableStartingLetters = new List<char>(_wordsByFirstLetter.Keys);
			availableStartingLetters = availableStartingLetters.Where(c => !_currentEnemies.Select (e => e.OriginalWord [0]).Concat(words.Select(w => w.ToUpper()[0])).Contains(c)).OrderBy(c => Random.Range(0, 200)).ToList();
			var maxWordScore = spawnData.MaxScore - nextTotalScore;
			//foreach (var startingLetter in 
			bool didPickWord = false;
			foreach (var startingLetter in availableStartingLetters)
			{
				string word;
				didPickWord = PickRandomWord (startingLetter, 9, maxWordScore, out word);
				if (didPickWord)
				{
					words.Add(word);
					Debug.Log("Picking " + word + " for score " + maxWordScore);
					nextTotalScore += WordUtils.GetWordScore(word);
					break;
				}
			}
			if (!didPickWord)
			{
				Debug.LogWarning("Couldn't find word for score: " + maxWordScore);
					break;
			}
		} while (nextTotalScore < spawnData.MaxScore);

		for (int i = 0; i < words.Count; i++) {
			var position = _enemySpawnPointPivots [i % _enemySpawnPointPivots.Length].position;
			var sprite = spawnData.Sprites [Random.Range (0, spawnData.Sprites.Length)];
			SpawnEnemy(words[i], position, Random.Range(spawnData.MaxMinionDuration, spawnData.MaxMinionDuration), sprite);
		}
	}

	private void RecalculatePlayerPositionByTarget()
	{
		if (_currentTarget == null) 
		{
			return;
		}
		var playerPosition = _playerPivot.Position;
		var targetX = Mathf.Lerp(playerPosition.x, _currentTarget.View.Position.x, 10 * Time.deltaTime);
		playerPosition.x = targetX;
		_playerPivot.Position = playerPosition;
	}

	protected void Update()
	{
		if (!_gameInProgress) {
			return;
		}
		SpawnNextEnemyOnUpdate ();
		ResolveEnemyTurnOnUpdate ();
		RecalculatePlayerPositionByTarget ();
		//if (Time.frameCount % 3 == 0) {
		SortEnemyLabels ();
		//}
		UpdateDisplay ();
		if (_currentTarget != null) 
		{
			if (_currentTarget.View.Position.x > _playerPivot.Position.x) {
				_playerPivot.FaceRight = true;
			} else if (_currentTarget.View.Position.x < _playerPivot.Position.x) {
				_playerPivot.FaceRight = false;
			}

		}
	}

	private void ResolveEnemyTurnOnUpdate()
	{
		for (int i = _currentEnemies.Count - 1; i >= 0; i--) 
		{
			var currentEnemey = _currentEnemies [i];
			if (currentEnemey.DidHit) {
				currentEnemey.View.SetTextColor (Color.grey);
			}
			else if (currentEnemey == _currentTarget) {
				currentEnemey.View.SetTextColor (_mainTargetColor);
			} else {
				currentEnemey.View.SetTextColor (_regularEnemyColor);
			}
			var enemyAction = ResolveEnemyTurnOnUpdate (currentEnemey);
			switch (enemyAction) 
			{
			case EnemyActionType.Hit:
				DealPlayerDamage ();
				currentEnemey.DidHit = true;
				_playerPivot.Blink ();
				if (!_gameInProgress) {
					return;
				}
				break;
			case EnemyActionType.Dying:
			case EnemyActionType.Despawn:
				DestroyEnemy (i);
				continue;
			case EnemyActionType.Move:
			default:
				break;
			}
		}
	}

	private void AddInputPenalty()
	{
		_playerScore = Mathf.Max (0, _playerScore - 10);
		_audioManager.PlayMiss ();
		_onScreenKeyboard.Blink ();
		//_penaltyEndTime = CurrentGameTime + 0.2f;
	}

	private EnemyActionType ResolveEnemyTurnOnUpdate(Enemy enemy)
	{
		if (_freezeEnemies) {
		//	return EnemyActionType.Frozen;
		}
		if (enemy.IsDead) 
		{
			return EnemyActionType.Dying;
		}
		var enemyLifetime = CurrentGameTime - enemy.SpawnTime;
		Vector3 enemyPosition;
		float enemyExitLifetime = 0;
		EnemyActionType enemyAction = EnemyActionType.Move;
		if (enemyLifetime < enemy.AttackDuration) 
		{
			enemyPosition = Vector3.Lerp (enemy.StartPosition, GetPlayerPosition (), enemyLifetime / enemy.AttackDuration);
			enemyAction = EnemyActionType.Move;
			enemy.View.Position = Vector3.MoveTowards(enemy.View.Position, enemyPosition, Time.deltaTime * 30);
		} 
		else
		{

		    enemyExitLifetime = enemyLifetime - enemy.AttackDuration;
			enemyPosition = Vector3.Lerp (GetPlayerPosition (), _enemyExitPivot.position, enemyExitLifetime / _enemyExitDuration);
			if (!enemy.DidHit) 
			{
				enemyAction = EnemyActionType.Hit;
			}
			else if (enemyExitLifetime > _enemyExitDuration) 
			{
				enemyAction = EnemyActionType.Despawn;
			}
			enemy.View.Position = enemyPosition;
		}

		return enemyAction;
	}

	private List<Rect> _placedRects = new List<Rect> ();
	private void SortEnemyLabels()
	{
		Vector3 avgEnemiesPosition = Vector3.zero;
		foreach (var enemyPosition in _currentEnemies.Where(e => !e.IsDead && !e.DidHit).Select (e => e.View.Position)) {
			avgEnemiesPosition += enemyPosition;
		}
		avgEnemiesPosition /= _currentEnemies.Count;
		foreach (var enemy in _currentEnemies.OrderBy(e => Vector3.Distance(e.View.Position, avgEnemiesPosition))) 
		{
			if (enemy.IsDead)// || enemy.DidHit) 
			{
				continue;
			}
			var deltaToAvg = enemy.View.Position - avgEnemiesPosition;
			deltaToAvg.z = 0;
			if (deltaToAvg.magnitude < 3f) 
			{
				deltaToAvg = Vector3.up * Mathf.Sign(deltaToAvg.magnitude);
			}
			var enemyDeltaFromAvg = deltaToAvg.normalized;

			//enemyDeltaFromAvg = Vector3.up;
			var initialCanvasPosition = enemy.View.CanvasPosition;
			enemy.View.CanvasPosition = avgEnemiesPosition + deltaToAvg;
			while (IsColliding (enemy.View.GetCanvasRect (), _placedRects)) 
			{
				enemy.View.CanvasPosition += enemyDeltaFromAvg;
			}
			var targetCanvasPosition = enemy.View.CanvasPosition;
			enemy.View.CanvasPosition = Vector3.Lerp (initialCanvasPosition, targetCanvasPosition, 5 * Time.deltaTime);
			_placedRects.Add (enemy.View.GetCanvasRect ());
		}
		_placedRects.Clear ();
	}

	private bool IsColliding(Rect a, List<Rect> others)
	{
		foreach (var other in others) 
		{
			if (IsColliding (a, other)) {
				return true;
			}
		}
		return false;
	}

	private bool IsColliding(Rect a, Rect b)
	{
		return (a.xMin < b.xMax && a.xMax > b.xMin && a.yMin < b.yMax && a.yMax > b.yMin);
	}

	private void SpawnEnemy(string word, Vector3 position, float attackDuration, Sprite sprite)
	{
		var enemyScore = WordUtils.GetWordScore (word);
		var enemy = new Enemy () {
			OriginalWord = word,
			Word = word,
			SpawnTime = CurrentGameTime,
			AttackDuration = attackDuration,
			StartPosition = position,
			View = Instantiate(_enemyViewPrefab),
		};
		enemy.View.SetWobbleDuration (attackDuration / 20f);
		enemy.View.SetDisplaySprite (sprite);
		enemy.View.SetDisplayText (word);
		enemy.View.Position = position;
		_currentEnemies.Add (enemy);	
	}

	private void DealPlayerDamage()
	{
		if (PlayerHp > 0) 
		{
			_audioManager.PlayPlayerHit ();
			PlayerHp -= 1;
			_penaltyEndTime = CurrentGameTime + 0.2f;
		}
		if (PlayerHp <= 0) 
		{
			EndGame ();
		}

	}

	private IEnumerator EndGameCoroutine()
	{
		for (int i = 0; i < 30; i++) 
		{
			int num = Random.Range(0, 26); // Zero to 25
			char letter = (char)('a' + num);
			_playerPivot.EmitGib (letter.ToString ());
			_audioManager.PlayPlayerHit ();
			yield return new WaitForSeconds (0.1f);
		}
		DespawnAllEnemies ();
		GameEnded ();
	}


	private Vector3 GetRandomEnemySpawnPosition()
	{
		if (_enemySpawnPointPivots == null || _enemySpawnPointPivots.Length == 0) 
		{
			throw new Exception ("No enemy spawn points defined");
		}
		return _enemySpawnPointPivots [Random.Range (0, _enemySpawnPointPivots.Length)].position;
	}

	private Vector3 GetPlayerPosition()
	{
		return _playerPivot.Position;
	}

	private Enemy TryPickEnemyByFirstLetter(string input)
	{
		return _currentEnemies.Where(e => !e.DidHit && !e.IsDead).FirstOrDefault(e => e.Word.ToUpper().StartsWith(input.ToUpper()));
	}

	private bool TryHitEnemy(Enemy target, string input)
	{		
		if (target.Word.ToUpper().StartsWith (input.ToUpper()))
		{
			char taken = target.Word [0];
			target.Word = target.Word.Substring (input.Length);
			var writtenWordPart = target.OriginalWord.Substring (0, target.OriginalWord.Length - target.Word.Length);
			target.View.SetDisplayText ("<color=grey>" + writtenWordPart + "</color>" + target.Word);
			target.View.EmitGib (taken.ToString());
			return true;
		}
		return false;
	}

	private void PositionPlayer(Vector3 position, float duration)
	{
		StartCoroutine (PositionPlayerCoroutine (position, duration));
	}

	private IEnumerator PositionPlayerCoroutine(Vector3 position, float duration)
	{
		_playerPivot.PlayMeleeAttack ();
		if (_freezeEnemies) {
			yield break;
		}
		_freezeEnemies = true;
		var origPos = _playerPivot.Position;
		_playerPivot.Position = position;
		yield return new WaitForSeconds (duration);
		_playerPivot.Position = origPos;
		_freezeEnemies = false;
		_playerPivot.PlayIdle ();
	}

	private bool DestroyEnemy(Enemy target)
	{
		for (int i = _currentEnemies.Count - 1; i >= 0; i--) 
		{
			var enemy = _currentEnemies [i];
			if (enemy == target) 
			{
				DestroyEnemy (i);
				return true;
			}
		}
		return false;
	}

	private void DestroyEnemy(int index)
	{
		var enemy = _currentEnemies [index];
		if (enemy == _currentTarget) 
		{
			_currentTarget = null;
		}
		_currentEnemies.RemoveAt (index);
		Destroy (enemy.View.gameObject);
	}

	private void OnKeyboardInput(string input)
	{
		
		if (CurrentGameTime < _penaltyEndTime) 
		{
			return; // In penalty
		}
		if (_currentTarget != null && (_currentTarget.DidHit || _currentTarget.IsDead)) {
			_currentTarget = null;
		}
		if (_currentTarget == null) 
		{
			_currentTarget = TryPickEnemyByFirstLetter (input);
			if (_currentTarget == null) 
			{
				AddInputPenalty ();

				return;
			}
		}
		bool didHit = TryHitEnemy (_currentTarget, input);
		if (!didHit) {
			AddInputPenalty ();
		} 
		else 
		{
			var enemyLifetime = CurrentGameTime - _currentTarget.SpawnTime;

			if (!_samuraiMode || enemyLifetime < _currentTarget.AttackDuration/2) {
				var projectile = Instantiate (_projectilePrefab);
				projectile.Shoot (_playerPivot.ProjectileEmissionPosition, _currentTarget.View.Position);
				_playerPivot.PlayRangedAttack ();
				_audioManager.PlayRangedHit ();
			}
			else 
			{
				_audioManager.PlayKick ();
				PositionPlayer (_currentTarget.View.Position - Vector3.up * 5f, 0.5f);
				_cameraXShake.Shake (0.5f);
			}
			// Enemy was killed?
			if (_currentTarget.Word == string.Empty) 
			{
				var enemyScore = WordUtils.GetWordScore (_currentTarget.OriginalWord);
				_playerScore += enemyScore;
				HighScoreUtils.WriteIfHighScore (_playerScore);
				_cameraYShake.Shake (0.5f);
				var enemyBody = Instantiate (_enemyBodyViewPrefab);
				enemyBody.SetSprite (_currentTarget.View.Sprite);
				enemyBody.Shoot (_currentTarget.View.Position);
				DestroyEnemy (_currentTarget);
				_currentTarget = null;
			}
		}
	}
	private void UpdateDisplay()
	{
		var targetWord = _currentTarget == null ? string.Empty : _currentTarget.Word;
		var targetOriginalWord = _currentTarget == null ? string.Empty : _currentTarget.OriginalWord;
		_scoreCounter.SetTargetValue (_playerScore);
		var writtenWordPart = targetOriginalWord.Substring (0, targetOriginalWord.Length - targetWord.Length);
		_onScreenKeyboard.SetDisplayText ("<color=grey>" + writtenWordPart + "</color>" + targetWord);
	}

	private bool PickRandomStartingLetter(List<char> exclude, out char chosen)
	{
		var letters = new List<char> (_wordsByFirstLetter.Keys.Where(c => !exclude.Contains(c)));
		if (letters.Count == 0) {
			chosen = '\n';
			return false;
		}
		var randomIndex = Random.Range (0, letters.Count);
		chosen = letters [randomIndex];
		return true;
	}

	private bool PickRandomWord(char letter, int minScore, int maxScore, out string word)
	{
		var words = _wordsByFirstLetter [letter].Where(w => WordUtils.GetWordScore(w) <= maxScore && WordUtils.GetWordScore(w) >= minScore).OrderBy(w => WordUtils.GetWordScore(w) + Random.Range(-100,100)).Reverse().ToList();
		if (words.Count == 0) 
		{
			word = null;
			return false;
		}
		//word = words [Random.Range (0, words.Count)];
		word = words [0];

		return true;
	}

	private void OnBackspacePressed()
	{
		_currentTarget = null;
		//_currentInputWord.DeleteLastSegment ();
		//_onScreenKeyboard.SetDisplayText (_currentInputWord.Text);
	}

}
