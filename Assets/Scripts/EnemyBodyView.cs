﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EnemyBodyView : MonoBehaviour
{
	[SerializeField]
	private SpriteRenderer _spriteRenderer = null;

	[SerializeField]
	private Transform _rotatePivot = null;

	private float _radialSpeed = 0;

	[SerializeField]
	private Transform _scalePivot = null;

	[SerializeField]
	private float _prefadeDelay = 3;

	[SerializeField]
	private AnimationCurve _fadeOutCurve = AnimationCurve.Linear(0,0,1,1);


	private float _floorTrackingSpeed = 6f;

	private bool _scrollWithFloor;
	// Update is called once per frame
	void Update () 
	{
		if (_scrollWithFloor) {
			transform.position -= Vector3.up * Time.deltaTime * _floorTrackingSpeed;
		}
	}

	public void SetSprite(Sprite sprite)
	{
		_spriteRenderer.sprite = sprite;
	}

	public void Shoot(Vector3 startPosition)
	{
		var landPosition = startPosition + Vector3.right * Random.Range (-2, 2)+ Vector3.up * Random.Range (-1, 1);
		StartCoroutine(ShootCoroutine(startPosition, landPosition, Random.Range(0.5f, 1.2f), Random.Range(2, 6), 2f));
	}

	private IEnumerator ShootCoroutine(Vector3 source, Vector3 target, float duration, float height, float fadeoutDuration)
	{
		var color = _spriteRenderer.color;
		var startTime = Time.time;
		float elapsed = 0;
		var scale = Vector3.one;
		while (elapsed <= duration) 
		{
			var progress = elapsed / duration;
			var position = Vector3.Lerp (source, target, progress) + Vector3.up * Mathf.Sin (progress * 180 * Mathf.Deg2Rad) * height;
			transform.position = position;
			elapsed += Time.deltaTime;
			_rotatePivot.Rotate(new Vector3(0, 0, _radialSpeed*Time.deltaTime));
			scale.y = 1 - progress / 2;
			_scalePivot.localScale = scale;
			yield return null;
		}
		_scrollWithFloor = true;
		yield return new WaitForSeconds (_prefadeDelay);
		elapsed = 0;
		startTime = Time.time;
		while (elapsed <= duration) {
			var progress = elapsed / duration;
			color.a = 1 - _fadeOutCurve.Evaluate(progress);
			elapsed += Time.deltaTime;
			_spriteRenderer.color = color;
			yield return null;
		}
		Destroy (gameObject);
	}

	// Use this for initialization
	void Start () {
		_radialSpeed = Random.Range (-360, 360);
	}

}
