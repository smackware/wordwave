﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {
	[SerializeField]
	private AudioSource _musicAudioSource = null;

	[SerializeField]
	private AudioSource _audioSource = null;

	[SerializeField]
	private AudioClip[] _keyHitClips = null;

	[SerializeField]
	private AudioClip[] _keyMissClips = null;

	[SerializeField]
	private AudioClip[] _kickClips = null;

	[SerializeField]
	private AudioClip[] _playerHits = null;

	public bool IsEnabled { get; set; }

	public void PlayPlayerHit()
	{
		PlayRandomClip (_playerHits);
	}
		
	public void PlayRangedHit()
	{
		PlayRandomClip (_keyHitClips);
	}

	public void PlayKick()
	{
		PlayRandomClip (_kickClips);
	}

	public void PlayMiss()
	{
		PlayRandomClip (_keyMissClips);
	}

	public void PlayMusic(bool value)
	{
		if (value) {
			if (!IsEnabled)
				return;
			_musicAudioSource.Play ();
		} else {
			_musicAudioSource.Stop ();
		}
	}

	private void PlayRandomClip(AudioClip[] clips)
	{
		if (!IsEnabled)
			return;
		_audioSource.clip = clips [Random.Range (0, clips.Length)];
		_audioSource.Play ();
	}
}
