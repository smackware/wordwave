﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WaveAnnouncer : MonoBehaviour 
{
	[SerializeField]
	private Text _title = null;

	private void Start()
	{
		gameObject.SetActive (false);
	}

	public void Show(string text)
	{
		_title.text = text;
		gameObject.SetActive (true);
		StartCoroutine (ShowCoroutine ());
	}

	private IEnumerator ShowCoroutine()
	{
		var canvasGroup = GetComponent<CanvasGroup> ();
		var startTime = Time.time;
		float elapsed = 0;
		var duration = 2;
		while (elapsed <= duration) {
			elapsed += Time.deltaTime;
			canvasGroup.alpha = 1 - elapsed / duration;
			yield return null;
		}
		canvasGroup.alpha = 0;
		gameObject.SetActive (false);
	}


}
