﻿using UnityEngine;
using System.Collections;

public class FloorScroller : MonoBehaviour {

	[SerializeField]
	private Renderer _renderer;

	[SerializeField]
	private float _speed = 5;


	// Update is called once per frame
	void Update () {
		_renderer.material.SetTextureOffset ("_MainTex", new Vector2(0 , Time.time * _speed));
	}
}
