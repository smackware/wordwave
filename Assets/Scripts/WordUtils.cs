﻿using System.Linq;

public static class WordUtils  
{
	public static int GetWordScore(string word)
	{
		return word.Distinct ().Count () * word.Length;
	}

}
