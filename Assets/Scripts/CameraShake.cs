﻿using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour {

	/// <summary>
	/// Target axis for shaking
	/// </summary>
	[System.Serializable]
	public enum TargetAxisType
	{
		X,
		Y,
		Z,
	}

	[Tooltip("The target axis to shake")]
	public TargetAxisType Target;

	[Tooltip("The duration of a single shake, should be much less than the total tween duration")]
	public float CycleDuration;

	[Tooltip("What is the shake distance when the magnitude curve is 1")]
	public float MaxShakeMagnitude = 1;

	[Tooltip("Curve that multiplies the shake magnitude over time")]
	public AnimationCurve MagnitudeCurve = AnimationCurve.Linear(1,1, 1, 1);

	[Tooltip("The movement shape of a single shake")]
	public AnimationCurve CycleCurve = AnimationCurve.EaseInOut(0,0,0,0);

	public void Shake(float duration)
	{
		StartCoroutine (ShakeCoroutine (duration));
	}

	private IEnumerator ShakeCoroutine(float duration)
	{
		float startTime = Time.time;
		float elapsed = 0;
		Vector3 originalPosition = transform.position;
		while (elapsed <= duration) 
		{
			elapsed = Time.time - startTime;
			ApplyToTransform (transform, elapsed / duration);
			yield return null;
		}
		transform.position = originalPosition;
	}

	protected void ApplyToTransform(Transform transform, float tweenProgress)
	{
		var currentCycleT = CycleCurve.Evaluate((tweenProgress % CycleDuration) / CycleDuration) * MagnitudeCurve.Evaluate(tweenProgress);
		transform.localPosition = GetShakePosition(currentCycleT);
	}

	public Vector3 GetShakePosition(float value)
	{
		switch (Target)
		{
		case TargetAxisType.X:
			return new Vector3(value, 0, 0);
		case TargetAxisType.Y:
			return new Vector3(0, value, 0);
		case TargetAxisType.Z:
			return new Vector3(0, 0, value);
		}
		return Vector3.zero;
	}

}
