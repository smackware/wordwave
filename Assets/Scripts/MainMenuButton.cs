﻿using System;
using System.Collections;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public enum MainMenuButtonType
{
	NewGame,
	Sound,
	Quit,
	DismissTappedPopup,
	None, // Decorative
}

public class MainMenuButton : MonoBehaviour, IPointerClickHandler
{
	public event Action Tapped = delegate {};

	[SerializeField]
	private MainMenuButtonType _type;

	[SerializeField]
	private string _text = string.Empty;
		
	[SerializeField]
	private Text _displayText = null;

	[SerializeField]
	private EnemyGibView _gibPrefab = null;

	public void OnPointerClick (PointerEventData eventData)
	{
		Tapped ();
	}

	public MainMenuButtonType ButtonType
	{
		get {
			return _type;
		}
	}

	public void EmitGib(string text)
	{
		var gibView = Instantiate (_gibPrefab);
		gibView.SetText (text);
		gibView.Shoot (transform.position);
	}


	public Color Color
	{
		get {
			return _displayText.color;
		}
		set {
			_displayText.color = value;
		}
	}

	public string CurrentText
	{
		get {
			return _displayText.text;
		}
		set {
			_displayText.text = value;
		}
	}

	public string OriginalText
	{
		get
		{
			return _text;
		}
		set {
			_text = value;
		}
	}
}
