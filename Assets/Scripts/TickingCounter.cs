﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;



public class TickingCounter : MonoBehaviour {

	[SerializeField]
	private Text _text = null;

	private int _targetValue;
	private float _currentValue = 0;

	public void SetTargetValue(int targetValue)
	{
		_targetValue = targetValue;
	}
		
	// Update is called once per frame
	void Update ()
	{
		var deltaAbs = Mathf.Max(Mathf.Abs (_targetValue - _currentValue), 50);
		_currentValue = Mathf.MoveTowards (_currentValue, _targetValue, deltaAbs * Time.deltaTime);
		_text.text = ((int)(_currentValue)).ToString();
	}
}
